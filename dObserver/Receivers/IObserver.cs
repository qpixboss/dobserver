﻿using dObserver.Utils;

namespace dObserver.Receivers {
	/// <summary>
	/// Интерфейс приёмника данных
	/// </summary>
	interface IObserver {
		void update(InternalDataFormat data);
	}
}
