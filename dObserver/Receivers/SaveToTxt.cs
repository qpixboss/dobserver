﻿using System;
using System.IO;
using System.Text;

using dObserver.Sources;
using dObserver.Utils;

namespace dObserver.Receivers {
	/// <summary>
	/// Класс наблюдателя, сохраняющего расшифрованный пакет данных в текстовый файл
	/// </summary>
	class SaveToTxt : IObserver {
		IObservable source;

		StringBuilder path;
		string folderName = "txt";

		public SaveToTxt(IObservable obs) {
			source = obs;
			source.registerObserver(this);
			path = new StringBuilder();

			if (!Directory.Exists(folderName)) {
				Directory.CreateDirectory(folderName);
			}
		}

		public void update(InternalDataFormat data) {
			string package = data.Message;
			int packageNumber = data.PackageNumber;

			if (package == "") {
				ShowMessages.showMessage("В пакете № " +
					packageNumber + " нет сообщений МКИО!");
				return;
			}

			ShowMessages.showMessage("Сохраняю пакет № " + packageNumber.ToString());
			// Формируем имя файла пакета
			path.Append(folderName + "\\" + "Пакет №" + packageNumber.ToString() +
				" " + DateTime.Now.ToString("HH-mm-ss") + ".txt");

			// Записываем
			using (StreamWriter writer = new StreamWriter(path.ToString(), true)) {
				writer.WriteLine(package);
			}

			path.Clear();
		}
	}
}
