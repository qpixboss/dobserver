﻿using System.IO;

using dObserver.Sources;
using dObserver.Utils;

namespace dObserver.Receivers {
	/// <summary>
	/// Класс наблюдателя, сохраняющего полученную датаграмму в бинарный файл
	/// </summary>
	class SaveToBinaryFile : IObserver {
		IObservable source;

		string fileName = "\\binary.bin";
		string folderName = "binary";

		public SaveToBinaryFile(IObservable obs) {
			source = obs;
			source.registerObserver(this);

			if (!Directory.Exists(folderName)) {
				Directory.CreateDirectory(folderName);
			}
		}

		public void update(InternalDataFormat data) {
			ShowMessages.showMessage("Сохраняю пакет № " +
				data.PackageNumber.ToString() +
				" в бинарном виде");

			// Записываем в бинарный файл
			using (FileStream fstream = new FileStream(folderName + fileName, FileMode.Append)) {
				fstream.Write(data.Dgram, 0, data.Dgram.Length);
			}
		}
	}
}
