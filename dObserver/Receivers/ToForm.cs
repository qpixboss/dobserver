﻿using System;

using dObserver.Utils;
using dObserver.Sources;

namespace dObserver.Receivers {
	/// <summary>
	/// Класс наблюдателя, выводящего полученные данные на форму
	/// </summary>
	class ToForm : IObserver {
		IObservable source;

		DataSender del;

		public ToForm(IObservable obs, DataSender sender) {
			source = obs;
			source.registerObserver(this);

			del = sender;
		}

		public void update(InternalDataFormat data) {
			del("Пакет № " + data.PackageNumber + Environment.NewLine +
				data.Package + Environment.NewLine, 2);
			del("Сообщения МКИО из пакета № " + data.PackageNumber +
				Environment.NewLine + data.Message + Environment.NewLine, 3);
		}
	}
}
