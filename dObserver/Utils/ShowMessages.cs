﻿using System;
using System.Windows.Forms;

namespace dObserver.Utils {
	/// <summary>
	/// Статический класс, содержащий статический метод для отображения сообщений
	/// </summary>
	public static class ShowMessages {
		/// <summary>
		/// Функция, отображающее сообщение в консоли, а так же в отдельном окне
		/// </summary>
		/// <param name="longMessage">Полное сообщение для отображения в консоли</param>
		/// <param name="shortMessage">Краткое сообщение для отображения в окне</param>
		/// <param name="error">Тип сообщения (0 - сообщение, 1 - ошибка)</param>
		/// <param name="enWindow">Отображать в окне</param>
		public static void showMessage(string longMessage, string shortMessage = "", bool error = false, bool enWindow = false) {
			if (!enWindow) {
				if (!error)
					Console.WriteLine(longMessage);
				else {
					Console.WriteLine("Ошибка: " + longMessage);
				}
			} else {
				if (!error) {
					Console.WriteLine(longMessage);
					MessageBox.Show(shortMessage, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
				} else {
					Console.WriteLine("Ошибка: " + longMessage);
					MessageBox.Show(shortMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}
	}
}
