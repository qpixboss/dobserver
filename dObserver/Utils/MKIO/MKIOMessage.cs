﻿using System;
using System.Text;
using System.Collections.Generic;

namespace dObserver.Utils.MKIO {
	/// <summary>
	/// Класс, описывающий сообщения МКИО разных форматов
	/// </summary>
	public class MKIOMessage {
		bool line;				// Линия
		byte format;			// Формат сообщения
		ushort commandWord;		// Командное слово
		ushort answerWord;		// Ответное слово
		ushort[] dataWords;		// Слова данных

		/// <summary>
		/// Получаем и задаём значение линии
		/// </summary>
		public int LINE {
			get { return line ? 1 : 0; }
			set { line = (value != 0) ? true : false; }
		}

		/// <summary>
		/// Получаем и задаём значение формата
		/// </summary>
		public int FORMAT {
			get { return format; }
			set { format = (value >= 1 && value <= 10) ? (byte)value : (byte)0; }
		}

		/// <summary>
		/// Получаем и задаём значение командного слова
		/// </summary>
		public ushort COMMAND_WORD {
			get { return commandWord; }
			set { commandWord = value; }
		}

		/// <summary>
		/// Получаем или задаём адрес оконечного устройства
		/// </summary>
		public int ADDRESS {
			get { return (commandWord & 0xFFFF) >> 11; }
			set { commandWord = (ushort)((commandWord & 0x07FF) | (value << 11)); }
		}

		/// <summary>
		/// Получаем и задаём направление передачи
		/// </summary>
		public int DIRECTION {
			get { return (commandWord & 0x0400) >> 10; }
			set { commandWord = (ushort)((commandWord & 0xFBFF) | (((ushort)value) & 0x0400)); }
		}

		/// <summary>
		/// Получаем и задаём подадресс оконечного устройства
		/// </summary>
		public int SUB_ADDRESS {
			get { return (commandWord & 0x03E0) >> 5; }
			set { commandWord = (ushort)((commandWord & 0x03E0) | (((ushort)value) & 0xFC1F)); }
		}

		/// <summary>
		/// Получаем и задаём число слов данных в сообщении
		/// </summary>
		public int DATA_WORDS_COUNT {
			get { return commandWord & 0x001F; }
			set { commandWord = (ushort)((commandWord & 0xFFE0) | (((ushort)value) & 0x001F)); }
		}

		/// <summary>
		/// Получаем и задаём ответное слово
		/// </summary>
		public int ANSWER_WORD {
			get { return answerWord; }
			set { answerWord = (ushort)value; }
		}

		private void createDataWords() {
			for (int i = 0; i < dataWords.Length; i++)
				dataWords[i] = (ushort)i;
		}

		public MKIOMessage(List<MkioWord> mkioWords, byte format) {
			if (format == 1) {
				// Формируем командное слово
				byte[] tempBytes = mkioWords[0].getBytes().ToArray();
				ushort kc = (ushort)(tempBytes[0] << 8 | tempBytes[1]);
				COMMAND_WORD = kc;

				FORMAT = format;

				// Слова данных
				dataWords = new ushort[DATA_WORDS_COUNT];
				for (int i = 1; i <= DATA_WORDS_COUNT; i++) {
					tempBytes = mkioWords[i].getBytes().ToArray();
					ushort cd = (ushort)(tempBytes[0] << 8 | tempBytes[1]);
					dataWords[i - 1] = cd;
				}

				// Формируем ответное слово
				tempBytes = mkioWords[mkioWords.Count - 1].getBytes().ToArray();
				ushort oc = (ushort)(tempBytes[0] << 8 | tempBytes[1]);
				ANSWER_WORD = oc;
			} else if (format == 4) {
				// Формируем командное слово
				byte[] tempBytes = mkioWords[0].getBytes().ToArray();
				ushort kc = (ushort)(tempBytes[0] << 8 | tempBytes[1]);
				COMMAND_WORD = kc;

				FORMAT = format;

				// Формируем ответное слово
				tempBytes = mkioWords[mkioWords.Count - 1].getBytes().ToArray();
				ushort oc = (ushort)(tempBytes[0] << 8 | tempBytes[1]);
				ANSWER_WORD = oc;
			} else if (format == 5) {
				// Формируем командное слово
				byte[] tempBytes = mkioWords[0].getBytes().ToArray();
				ushort kc = (ushort)(tempBytes[0] << 8 | tempBytes[1]);
				COMMAND_WORD = kc;

				FORMAT = format;

				// Формируем ответное слово
				tempBytes = mkioWords[1].getBytes().ToArray();
				ushort oc = (ushort)(tempBytes[0] << 8 | tempBytes[1]);
				ANSWER_WORD = oc;

				// Слово данных
				dataWords = new ushort[1];
				tempBytes = mkioWords[2].getBytes().ToArray();
				ushort cd = (ushort)(tempBytes[0] << 8 | tempBytes[1]);
				dataWords[0] = cd;
			}
			
		}

		public override string ToString() {
			StringBuilder result = new StringBuilder();

			result.Append("\tФормат " + format.ToString() + "\r" + Environment.NewLine);

			result.Append("Командное слово: ");
			result.Append(UtilFunctions.rString(COMMAND_WORD, 16));
			result.Append(Environment.NewLine);

			result.Append("Состав командного слова:\n");
			result.Append("\tАдрес ОУ: ");
			result.Append(UtilFunctions.rString(ADDRESS, 5));
			result.Append(Environment.NewLine);

			result.Append("\tНаправление передачи: ");
			result.Append(DIRECTION.ToString());
			result.Append(Environment.NewLine);

			result.Append("\tПодадрес: ");
			result.Append(UtilFunctions.rString(SUB_ADDRESS, 5));
			result.Append(Environment.NewLine);

			result.Append("\tЧисло СД: ");
			result.Append(UtilFunctions.rString(DATA_WORDS_COUNT, 5));
			result.Append(" " + DATA_WORDS_COUNT);
			result.Append(Environment.NewLine + Environment.NewLine);

			result.Append("Слова данных:" + Environment.NewLine);
			if (dataWords.Length > 0) {
				for (int i = 0; i < dataWords.Length; i++) {
					result.Append(UtilFunctions.rString(dataWords[i], 16) + " " + dataWords[i]);
					result.Append(Environment.NewLine);
				}
				result.Append(Environment.NewLine);
			}

			result.Append("Ответное слово: ");
			result.Append(UtilFunctions.rString(ANSWER_WORD, 16));
			result.Append(Environment.NewLine + Environment.NewLine);

			if (FORMAT == 5) {
				result.Append(UtilFunctions.rString(dataWords[0], 16) + " " + dataWords[0]);
				result.Append(Environment.NewLine);
			}

			return result.ToString();
		}
	}
}
