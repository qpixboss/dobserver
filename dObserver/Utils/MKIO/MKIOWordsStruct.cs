﻿namespace dObserver.Utils.MKIO {
	/// <summary>
	/// Структура слова КПИ-1
	/// </summary>
	struct MKIOWordsStruct {
		public byte word1;	// Слово 1
		public byte word2;	// Слово 2
		public byte type;	// Тип

		public MKIOWordsStruct(byte word1, byte word2, byte type) {
			this.word1 = word1;
			this.word2 = word2;
			this.type = type;
		}
	}
}
