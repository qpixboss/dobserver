﻿using System.Collections.Generic;

namespace dObserver.Utils.MKIO {
	/// <summary>
	/// Класс, описывающий слово МКИО
	/// </summary>
	public class MkioWord {
		private List<byte> bytes;	// Список байт
		private string type;		// Тип слова
		private byte typeNumber;	// 0 - КС, 1 - ОС, 2 - СД, 3 - КС/ОС
		private ushort numberLine;	// Номер линии

		/// <summary>
		/// Строковый тип слова МКИО
		/// </summary>
		public string Type {
			get {
				return type;
			}

			set {
				type = value;
			}
		}

		/// <summary>
		/// Числовой тип слова МКИО
		/// </summary>
		public byte TypeNumber {
			get {
				return typeNumber;
			}

			set {
				typeNumber = value;
				switch (TypeNumber) {
					case 0:
						Type = "КС";
						break;
					case 1:
						Type = "ОС";
						break;
					case 2:
						Type = "СД";
						break;
				}
			}
		}

		/// <summary>
		/// Номер линии
		/// </summary>
		public ushort NumberLine {
			get {
				return numberLine;
			}
		}

		public List<byte> getBytes() { return bytes; }

		public MkioWord() { }

		public MkioWord(List<byte> b) {
			bytes = new List<byte>(b);
			if (bytes.Count == 6) {
				Type = "КС/ОС";
				TypeNumber = 3;
			} else {
				Type = "СД";
				TypeNumber = 2;
			}
			if ((bytes[3] & (1 << 7)) != 0) numberLine = 1; else numberLine = 0;
		}
	}
}
