﻿using System.Collections.Generic;
using System.Text;

namespace dObserver.Utils {
	/// <summary>
	/// Класс конфигурации системы
	/// </summary>
	class Config {
		private int port;				// Порт устройва
		private bool enTimer;			// Принимаем ли по таймеру
		private uint time;				// Сколько секунд принимаем
		private byte nSource;			// Номер источника
		private List<byte> nReceiver;	// Список номеров приёмников. Если список пустой, то выводим толко на форму

		/// <summary>
		/// Порт
		/// </summary>
		public int Port {
			get {
				return port;
			}

			set {
				port = value;
			}
		}

		/// <summary>
		/// Включён ли таймер
		/// </summary>
		public bool EnTimer {
			get {
				return enTimer;
			}

			set {
				enTimer = value;
			}
		}

		/// <summary>
		/// Сколько секунд принимаем
		/// </summary>
		public uint Time {
			get {
				return time;
			}

			set {
				time = value;
			}
		}

		/// <summary>
		/// Номер источника
		/// </summary>
		public byte NSource {
			get {
				return nSource;
			}

			set {
				nSource = value;
			}
		}

		/// <summary>
		/// Номер приёмника
		/// </summary>
		public List<byte> NReceiver {
			get {
				return nReceiver;
			}

			set {
				nReceiver = value;
			}
		}

		public Config(byte nSource, List<byte> nReceiver, int port = 0, bool enTimer = false, uint time = 0) {
			Port = port;
			EnTimer = enTimer;
			Time = time;
			NSource = nSource;
			NReceiver = nReceiver;
		}

		public override string ToString() {
			StringBuilder str = new StringBuilder();

			str.Append("Текущая конфигурация:\n\tПорт = " + Port.ToString() + "\n\tТаймер = " + EnTimer.ToString() +
				"\n\tКоличество секунд = " + Time.ToString() + "\n\tНомер источника = " + NSource.ToString() +
				"\n\tНомера приёмников = ");

			foreach (var receiver in nReceiver)
				str.Append(receiver + " ");

			return str.ToString();
		}
	}
}
