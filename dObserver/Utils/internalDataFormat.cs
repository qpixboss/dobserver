﻿namespace dObserver.Utils {
	/// <summary>
	/// Класс, для передачи данных между объектами
	/// </summary>
	class InternalDataFormat {
		private byte[] dgram;		// Исходная датаграмма
		private string package;		// Список слов МКИО
		private string message;		// Восстановленное сообщение
		private int packageNumber;  // Номер пакета

		/// <summary>
		/// Исходная датаграмма
		/// </summary>
		public byte[] Dgram {
			get { return dgram; }
			set { dgram = value; }
		}

		/// <summary>
		/// Список слов МКИО
		/// </summary>
		public string Package {
			get { return package; }
			set { package = value; }
		}

		/// <summary>
		/// Восстановленные сообщения
		/// </summary>
		public string Message {
			get { return message; }
			set { message = value; }
		}

		/// <summary>
		/// Номер пакета
		/// </summary>
		public int PackageNumber {
			get { return packageNumber; }
			set { packageNumber = value; }
		}

		public InternalDataFormat(byte[] dgram, string package, string message, int packageNumber) {
			Dgram = dgram;
			Package = package;
			Message = message;
			PackageNumber = packageNumber;
		}
	}
}
