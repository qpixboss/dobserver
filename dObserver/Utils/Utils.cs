﻿using System.Collections.Generic;
using System.Text;

namespace dObserver.Utils {
	public static class Devices {
		public static List<string> sources = new List<string>() { "КПИ-1", "Бинарный файл" };
		public static List<string> receivers = new List<string>() { "В txt", "В бинарный файл" };
	}

	public delegate void DataSender(string data, byte textboxNumber);

	public static class UtilFunctions {
		/// <summary>
		/// Возвращает список байт, определяющий двоичное представление числа
		/// </summary>
		/// <param name="i">Исходное число</param>
		/// <returns>Список байт, определяющий двоичное представление числа</returns>
		private static List<bool> getBinaryRepresentation(int i) {
			List<bool> result = new List<bool>();
			while (i > 0) {
				int m = i % 2;
				i = i / 2;
				result.Add(m == 1);
			}
			result.Reverse();
			return result;
		}

		/// <summary>
		/// Переводит число в двочиный вид (строковое представление)
		/// и дополняет нулями справа строку с двоичным представление числа
		/// </summary>
		/// <param name="input">Входное число</param>
		/// <param name="length">Нужная длина</param>
		/// <returns>Строка, содержащая двоичное представление числа</returns>
		public static string rString(int input, int length) {
			StringBuilder sb = new StringBuilder();

			List<bool> list = getBinaryRepresentation(input);
			if (list.Count < 16) {
				int realLength = list.Count;
				for (int i = 0; i < length - realLength; i++)
					list.Insert(0, false);
			}
			foreach (bool b in list)
				sb.Append(b ? "1" : "0");

			return sb.ToString();
		}
	}
}
