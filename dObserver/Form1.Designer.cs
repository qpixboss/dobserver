﻿namespace dObserver {
	partial class Form1 {
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent() {
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkedListBoxReceivers = new System.Windows.Forms.CheckedListBox();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonLoadConfig = new System.Windows.Forms.Button();
			this.buttonSaveConfig = new System.Windows.Forms.Button();
			this.textBoxTime = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.checkBoxTimer = new System.Windows.Forms.CheckBox();
			this.buttonStart = new System.Windows.Forms.Button();
			this.buttonSet = new System.Windows.Forms.Button();
			this.textBoxPort = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBoxSources = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.сохранитьКонфигурациюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.загрузитьКонфигурациюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.запуститьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkedListBoxReceivers);
			this.groupBox1.Controls.Add(this.buttonClear);
			this.groupBox1.Controls.Add(this.buttonLoadConfig);
			this.groupBox1.Controls.Add(this.buttonSaveConfig);
			this.groupBox1.Controls.Add(this.textBoxTime);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.checkBoxTimer);
			this.groupBox1.Controls.Add(this.buttonStart);
			this.groupBox1.Controls.Add(this.buttonSet);
			this.groupBox1.Controls.Add(this.textBoxPort);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.comboBoxSources);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(12, 27);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(223, 582);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Настройки";
			// 
			// checkedListBoxReceivers
			// 
			this.checkedListBoxReceivers.CheckOnClick = true;
			this.checkedListBoxReceivers.FormattingEnabled = true;
			this.checkedListBoxReceivers.Location = new System.Drawing.Point(82, 48);
			this.checkedListBoxReceivers.Name = "checkedListBoxReceivers";
			this.checkedListBoxReceivers.Size = new System.Drawing.Size(131, 94);
			this.checkedListBoxReceivers.TabIndex = 15;
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(9, 336);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(204, 23);
			this.buttonClear.TabIndex = 14;
			this.buttonClear.Text = "Очистить";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// buttonLoadConfig
			// 
			this.buttonLoadConfig.Location = new System.Drawing.Point(9, 278);
			this.buttonLoadConfig.Name = "buttonLoadConfig";
			this.buttonLoadConfig.Size = new System.Drawing.Size(204, 23);
			this.buttonLoadConfig.TabIndex = 13;
			this.buttonLoadConfig.Text = "Загрузить конфигурацию";
			this.buttonLoadConfig.UseVisualStyleBackColor = true;
			this.buttonLoadConfig.Click += new System.EventHandler(this.buttonLoadConfig_Click);
			// 
			// buttonSaveConfig
			// 
			this.buttonSaveConfig.Enabled = false;
			this.buttonSaveConfig.Location = new System.Drawing.Point(9, 249);
			this.buttonSaveConfig.Name = "buttonSaveConfig";
			this.buttonSaveConfig.Size = new System.Drawing.Size(204, 23);
			this.buttonSaveConfig.TabIndex = 12;
			this.buttonSaveConfig.Text = "Сохранить конфигурацию";
			this.buttonSaveConfig.UseVisualStyleBackColor = true;
			this.buttonSaveConfig.Click += new System.EventHandler(this.buttonSaveConfig_Click);
			// 
			// textBoxTime
			// 
			this.textBoxTime.Location = new System.Drawing.Point(82, 194);
			this.textBoxTime.Name = "textBoxTime";
			this.textBoxTime.Size = new System.Drawing.Size(131, 20);
			this.textBoxTime.TabIndex = 11;
			this.textBoxTime.Text = "1";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 197);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(77, 13);
			this.label6.TabIndex = 10;
			this.label6.Text = "Число секунд";
			// 
			// checkBoxTimer
			// 
			this.checkBoxTimer.AutoSize = true;
			this.checkBoxTimer.Location = new System.Drawing.Point(9, 177);
			this.checkBoxTimer.Name = "checkBoxTimer";
			this.checkBoxTimer.Size = new System.Drawing.Size(71, 17);
			this.checkBoxTimer.TabIndex = 9;
			this.checkBoxTimer.Text = "Таймер?";
			this.checkBoxTimer.UseVisualStyleBackColor = true;
			// 
			// buttonStart
			// 
			this.buttonStart.Enabled = false;
			this.buttonStart.Location = new System.Drawing.Point(9, 307);
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.Size = new System.Drawing.Size(204, 23);
			this.buttonStart.TabIndex = 7;
			this.buttonStart.Text = "Запустить";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
			// 
			// buttonSet
			// 
			this.buttonSet.Location = new System.Drawing.Point(9, 220);
			this.buttonSet.Name = "buttonSet";
			this.buttonSet.Size = new System.Drawing.Size(204, 23);
			this.buttonSet.TabIndex = 6;
			this.buttonSet.Text = "Установить";
			this.buttonSet.UseVisualStyleBackColor = true;
			this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
			// 
			// textBoxPort
			// 
			this.textBoxPort.Location = new System.Drawing.Point(82, 151);
			this.textBoxPort.Name = "textBoxPort";
			this.textBoxPort.Size = new System.Drawing.Size(131, 20);
			this.textBoxPort.TabIndex = 5;
			this.textBoxPort.Text = "4183";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 154);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(32, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Порт";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Приёмники";
			// 
			// comboBoxSources
			// 
			this.comboBoxSources.FormattingEnabled = true;
			this.comboBoxSources.Location = new System.Drawing.Point(82, 16);
			this.comboBoxSources.Name = "comboBoxSources";
			this.comboBoxSources.Size = new System.Drawing.Size(131, 21);
			this.comboBoxSources.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(55, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Источник";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.textBox3);
			this.groupBox2.Controls.Add(this.textBox2);
			this.groupBox2.Location = new System.Drawing.Point(241, 27);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(758, 582);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Данные";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(289, 28);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(159, 13);
			this.label5.TabIndex = 3;
			this.label5.Text = "Дешифрованная информация";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 29);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(40, 13);
			this.label4.TabIndex = 2;
			this.label4.Text = "Вывод";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(292, 45);
			this.textBox3.Multiline = true;
			this.textBox3.Name = "textBox3";
			this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox3.Size = new System.Drawing.Size(460, 546);
			this.textBox3.TabIndex = 1;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(6, 45);
			this.textBox2.Multiline = true;
			this.textBox2.Name = "textBox2";
			this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox2.Size = new System.Drawing.Size(277, 546);
			this.textBox2.TabIndex = 0;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.справкаToolStripMenuItem,
            this.выходToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1011, 24);
			this.menuStrip1.TabIndex = 2;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// файлToolStripMenuItem
			// 
			this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сохранитьКонфигурациюToolStripMenuItem,
            this.загрузитьКонфигурациюToolStripMenuItem,
            this.запуститьToolStripMenuItem});
			this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
			this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
			this.файлToolStripMenuItem.Text = "Файл";
			// 
			// сохранитьКонфигурациюToolStripMenuItem
			// 
			this.сохранитьКонфигурациюToolStripMenuItem.Enabled = false;
			this.сохранитьКонфигурациюToolStripMenuItem.Name = "сохранитьКонфигурациюToolStripMenuItem";
			this.сохранитьКонфигурациюToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
			this.сохранитьКонфигурациюToolStripMenuItem.Text = "Сохранить конфигурацию";
			// 
			// загрузитьКонфигурациюToolStripMenuItem
			// 
			this.загрузитьКонфигурациюToolStripMenuItem.Name = "загрузитьКонфигурациюToolStripMenuItem";
			this.загрузитьКонфигурациюToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
			this.загрузитьКонфигурациюToolStripMenuItem.Text = "Загрузить конфигурацию";
			this.загрузитьКонфигурациюToolStripMenuItem.Click += new System.EventHandler(this.загрузитьКонфигурациюToolStripMenuItem_Click);
			// 
			// запуститьToolStripMenuItem
			// 
			this.запуститьToolStripMenuItem.Enabled = false;
			this.запуститьToolStripMenuItem.Name = "запуститьToolStripMenuItem";
			this.запуститьToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
			this.запуститьToolStripMenuItem.Text = "Запустить";
			this.запуститьToolStripMenuItem.Click += new System.EventHandler(this.запуститьToolStripMenuItem_Click);
			// 
			// справкаToolStripMenuItem
			// 
			this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
			this.справкаToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
			this.справкаToolStripMenuItem.Text = "О программе";
			this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
			// 
			// выходToolStripMenuItem
			// 
			this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
			this.выходToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
			this.выходToolStripMenuItem.Text = "Выход";
			this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1011, 621);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "Приложение для работы в сети по протоколам МКИО";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ComboBox comboBoxSources;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxPort;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button buttonSet;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Button buttonStart;
		private System.Windows.Forms.TextBox textBoxTime;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox checkBoxTimer;
		private System.Windows.Forms.Button buttonSaveConfig;
		private System.Windows.Forms.Button buttonLoadConfig;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem сохранитьКонфигурациюToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem загрузитьКонфигурациюToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem запуститьToolStripMenuItem;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.CheckedListBox checkedListBoxReceivers;
	}
}

