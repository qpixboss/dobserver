﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;

using dObserver.Receivers;
using dObserver.Sources;
using dObserver.Sources.KPI1;
using dObserver.Utils;

namespace dObserver {
	public partial class Form1 : Form {

		IObservable source;
		List<IObserver> receivers;
		Config config;

		public Form1() {
			InitializeComponent();
			init();
		}

		// Для того, чтобы включить консоль
		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool AllocConsole();

		/// <summary>
		/// Инициализация компонентов формы
		/// </summary>
		private void init() {
			foreach (string s in Devices.sources) {
				comboBoxSources.Items.Add(s);
			}
			comboBoxSources.SelectedIndex = 0;

			foreach (string s in Devices.receivers) {
				checkedListBoxReceivers.Items.Add(s);
			}

			receivers = new List<IObserver>();

			AllocConsole();

			// Для работы из потоков таймера
			CheckForIllegalCrossThreadCalls = false;
		}

		/// <summary>
		/// Установка конфигурации системы
		/// </summary>
		/// <param name="iSource">Источник</param>
		/// <param name="iReceiver">Приёмник</param>
		/// <param name="port">Порт</param>
		/// <param name="timer">Включён ли таймер</param>
		/// <param name="time">Количество секунд</param>
		private void setConfig(byte iSource, List<byte> iReceiver, int port = 0, bool timer = false, uint time = 0) {
			receivers.Clear();
			if (source != null)
				source.Dispose();

			config = new Config(iSource, iReceiver, port, timer, time);
			Console.WriteLine(config.ToString());

			switch (config.NSource) {
				case 0:
					source = new KPI1(config);
					break;

				case 1:
					source = new BinaryFile(config);
					break;
			}

			foreach (var receiver in config.NReceiver) {
				switch (receiver) {
					case 0:
						receivers.Add(new SaveToTxt(source));
						break;

					case 1:
						receivers.Add(new SaveToBinaryFile(source));
						break;
				}
			}

			receivers.Add(new ToForm(source, new DataSender(dataHandler)));
		}

		/// <summary>
		/// Обработчик данных из других классов
		/// </summary>
		/// <param name="data">Текстовые данные для отображения</param>
		private void dataHandler(string data, byte textboxNumber) {
			switch (textboxNumber) {
				case 2:
					textBox2.AppendText(data);
					break;

				case 3:
					textBox3.AppendText(data);
					break;

				default:
					break;
			}
		}

		/// <summary>
		/// Сохранение конфигурации системы в XML файл.
		/// </summary>
		private void saveConfig() {
			if (!Directory.Exists("config"))
				Directory.CreateDirectory("config");

			// Формирование XML документа
			XDocument xmlConfig = new XDocument(
				new XElement("config",
				new XElement("port", config.Port),
				new XElement("enTimer", config.EnTimer),
				new XElement("time", config.Time),
				new XElement("nSource", config.NSource),
				from byte receiver in config.NReceiver select new XElement("nReceiver", receiver)));

			// Диалог сохранения XML документа
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Filter = "Файлы XML (*.xml) | *.xml";
			saveFileDialog.InitialDirectory = Environment.CurrentDirectory + "\\config\\";
			if (saveFileDialog.ShowDialog() == DialogResult.OK) {
				xmlConfig.Save(saveFileDialog.FileName);
				buttonSaveConfig.Enabled = false;
				сохранитьКонфигурациюToolStripMenuItem.Enabled = false;
			}
		}

		/// <summary>
		/// Загрузка конфигурации системы из XML файла.
		/// </summary>
		private void loadConfig() {
			// Диалог открытия XML документа
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Файлы XML (*.xml) | *.xml";
			openFileDialog.InitialDirectory = Environment.CurrentDirectory + "\\config\\";
			if (openFileDialog.ShowDialog() == DialogResult.OK) {
				// Загрузка файла конфигурации
				XDocument xmlConfig = XDocument.Load(openFileDialog.FileName);
				XElement conf = xmlConfig.Element("config");

				IEnumerable<XElement> items =
					from el in conf.Descendants("nReceiver")
					select el;

				List<byte> rec = new List<byte>();
				foreach (var el in items)
					rec.Add(byte.Parse(el.Value));

				setConfig(byte.Parse(conf.Element("nSource").Value),
					rec,
					int.Parse(conf.Element("port").Value),
					bool.Parse(conf.Element("enTimer").Value),
					uint.Parse(conf.Element("time").Value));

				// Установка данных на форме
				comboBoxSources.SelectedIndex = byte.Parse(conf.Element("nSource").Value);
				foreach (var el in items)
					checkedListBoxReceivers.SetItemChecked(int.Parse(el.Value), true);

				buttonStart.Enabled = true;
				запуститьToolStripMenuItem.Enabled = true;
			}
		}

		private void buttonSet_Click(object sender, EventArgs e) {
			int port;
			uint time;

			if (!int.TryParse(textBoxPort.Text, out port)) {
				ShowMessages.showMessage("Неверно установлен порт. Он должен иметь числовой вид",
					"Неверно установлен порт", true, true);
				return;
			}

			if (!uint.TryParse(textBoxTime.Text, out time)) {
				ShowMessages.showMessage("Неверно установлено время. Оно должен иметь числовой вид.",
					"Неверно установлено время", true, true);
				return;
			}

			List<byte> cIndices = new List<byte>();
			foreach (int ind in checkedListBoxReceivers.CheckedIndices)
				cIndices.Add((byte)ind);

			setConfig((byte)comboBoxSources.SelectedIndex, cIndices, port, checkBoxTimer.Checked, time);

			buttonStart.Enabled = true;
			запуститьToolStripMenuItem.Enabled = true;
			buttonSaveConfig.Enabled = true;
			сохранитьКонфигурациюToolStripMenuItem.Enabled = true;
		}

		private void buttonStart_Click(object sender, EventArgs e) {
			source.start();
		}

		private void buttonSaveConfig_Click(object sender, EventArgs e) {
			saveConfig();
		}

		private void buttonLoadConfig_Click(object sender, EventArgs e) {
			loadConfig();
		}

		private void выходToolStripMenuItem_Click(object sender, EventArgs e) {
			Application.Exit();
		}

		private void загрузитьКонфигурациюToolStripMenuItem_Click(object sender, EventArgs e) {
			loadConfig();
		}

		private void buttonClear_Click(object sender, EventArgs e) {
			textBox2.Clear();
			textBox3.Clear();
		}

		private void справкаToolStripMenuItem_Click(object sender, EventArgs e) {
			MessageBox.Show("Программа для работы в сети по протоколам МКИО" + Environment.NewLine + 
				"Выполнил студент группы 4ИПм-1 Деменин Д. Л." + Environment.NewLine +
				"Научный руководитель: Тихомиров В. А." + Environment.NewLine + 
				"Данная программа предназначена для работы в сети по протоколам МКИО. Пользователь выбирает источник и приёмники данных, " + 
				"указывает параметры и запускает приём данных. Результат работы алгоритмов разбора потоков отображается на форме," +
				" текстовых и бинарном файле.", "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void запуститьToolStripMenuItem_Click(object sender, EventArgs e) {
			source.start();
		}
	}
}
