﻿using System;
using System.Collections.Generic;
using System.Text;

using dObserver.Utils;
using dObserver.Utils.MKIO;

namespace dObserver.Sources.KPI1 {
	/// <summary>
	/// Класс, производящий анализ потока КПИ-1.
	/// Проводит проверку правильности последовательности байт, последовательности слов МКИО.
	/// Восстанавливает сообщения МКИО.
	/// Работает как с массивом входящих байт, так и со строками.
	/// </summary>
	public class FlowAnalysisKPI1 {
		private byte[] bytes;                   // Массив входных байтов
		private StringBuilder outText;          // Выходная текстовая строка
		private List<MKIOMessage> mkioMessage;  // Список сообщений МКИО в пакете

		// Временные списки
		private List<MKIOWordsStruct> mkio;
		private List<MkioWord> mkioWords;

		/// <summary>
		/// Пакет данных размера 1024, поступивший от КПИ-1
		/// </summary>
		public byte[] inputBytes {
			set {
				if (value.Length != 1024) {
					ShowMessages.showMessage("В пакете, пришедшем от КПИ-1 должно быть 1024 байта, а не " +
						value.Length, "В пакете должно быть 1024 байта", true, true);
					return;
				}
				bytes = value;
			}
		}

		/// <summary>
		/// Строка, содержащая пакет данных, поступивший от КПИ-1
		/// </summary>
		public string inputString {
			set {
				stringToBytes(value);
			}
		}

		/// <summary>
		/// Выходная строка, представляющая пакет
		/// </summary>
		public string outputString {
			get { return outText.ToString(); }
		}

		/// <summary>
		/// Список слов данных МКИО
		/// </summary>
		public List<MkioWord> outputMkioWords {
			get { return mkioWords; }
		}

		/// <summary>
		/// Список сообщений МКИО
		/// </summary>
		public List<MKIOMessage> MkioMessage {
			get { return mkioMessage; }
		}

		/// <summary>
		/// Конструкторк по умолчанию
		/// </summary>
		public FlowAnalysisKPI1() {
			bytes = new byte[1024];
			init();
		}

		/// <summary>
		/// Конструктор, принимающий на вход один пакет в виде массива байт
		/// </summary>
		/// <param name="bytes">Пакет от КПИ-1</param>
		public FlowAnalysisKPI1(byte[] bytes) {
			inputBytes = bytes;

			init();
		}

		/// <summary>
		/// Конструктор, принимающий на вход один пакет в виде строки
		/// </summary>
		/// <param name="stringOfBytes">Пакет от КПИ-1</param>
		public FlowAnalysisKPI1(string stringOfBytes) {
			stringToBytes(stringOfBytes);
			init();
		}

		/// <summary>
		/// Создание всех необходимых списков
		/// </summary>
		private void init() {
			mkio = new List<MKIOWordsStruct>();
			outText = new StringBuilder();
			mkioWords = new List<MkioWord>();
			mkioMessage = new List<MKIOMessage>();
		}

		/// <summary>
		/// Создание массива байт из строки
		/// </summary>
		/// <param name="s">Строка, содержащая массив байт</param>
		private void stringToBytes(string s) {
			int numOfBytes = s.Length / 8;
			if (numOfBytes != 1024) {
				ShowMessages.showMessage("В пакете, пришедшем от КПИ-1 должно быть 1024 байта, а не " + numOfBytes,
					"В пакете должно быть 1024 байта", true, true);
				return;
			}
			bytes = new byte[numOfBytes];
			for (int i = 0; i < numOfBytes; ++i)
				bytes[i] = Convert.ToByte(s.Substring(8 * i, 8), 2);
		}

		/// <summary>
		/// Очистка всех списков.
		/// </summary>
		private void clearAllLists() {
			outText.Clear();
			mkioMessage.Clear();
			mkio.Clear();
			mkioWords.Clear();
		}

		/// <summary>
		/// Начало разбора потока, поступающего от КПИ-1
		/// </summary>
		public void startAnalysis() {
			clearAllLists();
			chekBytes();
			mkioAnalysis();
			startMkioMessageRestoration();
		}

		/// <summary>
		/// Проверка порядка байт
		/// </summary>
		private void chekBytes() {
			outText.Clear();
			for (int i = 1; i < bytes.Length; i += 2) {
				bool bit = false;   // Текущий бит в байте
				int state = 0;      // Текущее состояние

				for (int bitOfByte = 7; bitOfByte >= 0; bitOfByte--) {
					// Выделение определённых битов в байте
					if ((bytes[i] & (1 << bitOfByte)) != 0) bit = true; else bit = false;
					state = statesBytes(state, bit, bitOfByte); // Получение нового состояния

					// После допуска последовательности добавляем два байта и тип в выходную строку
					// и формируем список частей слов МКИО
					switch (state) {
						case 42:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "ARINC 1" + Environment.NewLine);
							state = 0;
							break;

						case 421:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "ARINC 2" + Environment.NewLine);
							state = 0;
							break;

						case 422:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "ARINC 3" + Environment.NewLine);
							state = 0;
							break;

						case 423:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "Время 1" + Environment.NewLine);
							state = 0;
							break;

						case 424:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "Время 2" + Environment.NewLine);
							state = 0;
							break;

						case 425:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "Время 3" + Environment.NewLine);
							state = 0;
							break;

						case 426:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "МКИО 1" + Environment.NewLine);
							mkio.Add(new MKIOWordsStruct(bytes[i], bytes[i - 1], 1));
							state = 0;
							break;

						case 427:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "МКИО 2" + Environment.NewLine);
							mkio.Add(new MKIOWordsStruct(bytes[i], bytes[i - 1], 2));
							state = 0;
							break;

						case 428:
							outText.Append(bytes[i].ToString("X4") + " " + bytes[i - 1].ToString("X4") +
								" | " + "МКИО 3" + Environment.NewLine);
							mkio.Add(new MKIOWordsStruct(bytes[i], bytes[i - 1], 3));
							state = 0;
							break;

						default:
							break;
					}
				}
			}

			ShowMessages.showMessage("Автомат закончил работу");
		}

		/// <summary>
		/// Функция переходов в проверке порядка байт
		/// </summary>
		/// <param name="state">Текущее состояние</param>
		/// <param name="symb">Текущий символ</param>
		/// <param name="bitOfByte">Текущий бит в байте</param>
		/// <returns>Возвращает состояние или -1 в случае ошибки</returns>
		private int statesBytes(int state, bool symb, int bitOfByte) {
			switch (state) {
				case 0:
					// Начальное состояние
					if (!symb) return 1;
					break;

				case 1:
					if (symb) return 8; else return 2;

				case 2:
					if (symb) return 6; else return 3;

				case 3:
					if (symb) return 5; else return 4;

				case 4:
					// Допустить ARINC 1
					if (bitOfByte == 0) return 42; else return 4;

				case 5:
					// Допустить ARINC 2
					if (bitOfByte == 0) return 421; else return 5;

				case 6:
					if (!symb) return 7;
					break;

				case 7:
					// Допустить ARINC 3
					if (bitOfByte == 0) return 422; else return 7;

				case 8:
					if (symb) return 16; else return 9;

				case 9:
					if (!symb) return 10;
					break;

				case 10:
					if (symb) return 14; else return 11;

				case 11:
					if (symb) return 13; else return 12;

				case 12:
					// Допустить Время 1
					if (bitOfByte == 0) return 423; else return 12;

				case 13:
					// Допустить Время 2
					if (bitOfByte == 0) return 424; else return 13;

				case 14:
					if (!symb) return 15;
					break;

				case 15:
					// Допустить Время 3
					if (bitOfByte == 0) return 425; else return 15;

				case 16:
					if (!symb) return 17;
					break;

				case 17:
					if (symb) return 19; else return 18;

				case 18:
					// Допустить МКИО 1
					if (bitOfByte == 0) return 426; else return 18;

				case 19:
					if (symb) return 21; else return 20;

				case 20:
					// Допустить МКИО 2
					if (bitOfByte == 0) return 427; else return 20;

				case 21:
					// Допустить МКИО 3
					if (bitOfByte == 0) return 428; else return 21;

				default:
					break;
			}

			return -1;
		}

		/// <summary>
		/// Функция, собирающая слова МКИО из частей, поступивших от КПИ-1
		/// </summary>
		private void mkioAnalysis() {
			if (mkio.Count == 0) {
				ShowMessages.showMessage("Список частей слов МКИО пуст!", null, true);
				return;
			}

			List<byte> tempByte = new List<byte>();
			byte waitingWord = 1;

			// Пробегаем все части слов в списке
			foreach (MKIOWordsStruct item in mkio) {
				// и в зависимости от ожидаемой части слова
				// и пришедшей мы или добавляем байты в список b и ждём новую часть слова
				// или очищаем список и ищем новое командное слово
				switch (waitingWord) {
					case 1:
						if (item.type == waitingWord) {
							tempByte.Add(item.word1);
							tempByte.Add(item.word2);
							waitingWord = 2;
						} else {
							tempByte.Clear();
							waitingWord = 1;
						}
						break;

					case 2:
						if (item.type == waitingWord) {
							tempByte.Add(item.word1);
							tempByte.Add(item.word2);
							waitingWord = 3;
						} else {
							tempByte.Clear();
							waitingWord = 1;
						}
						break;

					case 3:
						if (item.type == waitingWord) {
							tempByte.Add(item.word1);
							tempByte.Add(item.word2);
							waitingWord = 1;
							mkioWords.Add(new MkioWord(tempByte));
							tempByte.Clear();
						} else if (item.type == 1) {
							mkioWords.Add(new MkioWord(tempByte));
							tempByte.Clear();
							tempByte.Add(item.word1);
							tempByte.Add(item.word2);
							waitingWord = 2;
						} else {
							tempByte.Clear();
							waitingWord = 1;
						}
						break;
				}
			}
		}

		/// <summary>
		/// Восстанавливаем сообщение МКИО и передаём его для получения данных в класс MKIOMessage
		/// </summary>
		private void startMkioMessageRestoration() {
			if (mkioWords.Count == 0) {
				ShowMessages.showMessage("Список слов МКИО пуст!", null, true);
				return;
			}

			List<MkioWord> tempMkioWords = new List<MkioWord>();
			int state = 0;
			int startPosition = 0;
			for (int i = 0; i < mkioWords.Count; i++) {

				state = mkioStates(state, mkioWords[i].TypeNumber, i);

				switch (state) {
					case 41:
						// Допускаем первый формат
						for (int j = startPosition; j < i; j++) {
							tempMkioWords.Add(mkioWords[j]);
						}
						mkioMessage.Add(new MKIOMessage(tempMkioWords, 1));
						tempMkioWords.Clear();
						state = 0;
						i--;
						startPosition = i + 1;
						break;
					case 44:
						// Допускаем четвёртый формат
						for (int j = startPosition; j < i; j++) {
							tempMkioWords.Add(mkioWords[j]);
						}
						mkioMessage.Add(new MKIOMessage(tempMkioWords, 4));
						tempMkioWords.Clear();
						state = 0;
						i--;
						startPosition = i + 1;
						break;
					case 45:
						// Допускаем пятый формат
						for (int j = startPosition; j < i; j++) {
							tempMkioWords.Add(mkioWords[j]);
						}
						mkioMessage.Add(new MKIOMessage(tempMkioWords, 5));
						tempMkioWords.Clear();
						state = 0;
						i--;
						startPosition = i + 1;
						break;
					case -1:
						// Ошибка
						tempMkioWords.Clear();
						state = 0;
						startPosition = i + 1;
						break;
					case -5:
						// Ошибка в пятом формате
						tempMkioWords.Clear();
						state = 0;
						for (int j = i; j > 0; j--)
							if (mkioWords[j].TypeNumber == 3) i = --j;
						startPosition = i + 1;
						break;
				}
			}
		}

		/// <summary>
		/// Функция переходов для конечного автомата восстановления сообщений
		/// </summary>
		/// <param name="state">Текущее состояние</param>
		/// <param name="wordType">Тип слова</param>
		/// <param name="numberOfWords">Текущая позиция</param>
		/// <returns>Новое состояние</returns>
		private int mkioStates(int state, int wordType, int numberOfWords) {
			switch (state) {
				case 0:
					if (wordType == 3) return 1;
					break;
				case 1:
					if (wordType == 2) return 2;
					if (wordType == 3) return 4;
					break;
				case 2:
					if (wordType == 2) return 2;
					if (wordType == 3 && numberOfWords == mkioWords.Count - 1) return 41;
					if (wordType == 3) return 3;
					break;
				case 3:
					// Формат 1
					if (wordType == 3) return 41;
					break;
				case 4:
					if (wordType == 2) return 5;
					// Формат 4
					if (wordType == 3 && numberOfWords == mkioWords.Count - 1) return 44;
					if (wordType == 3) return 44;
					break;
				case 5:
					// Формат 5
					if (wordType == 3) return 45;
					if (wordType == 2) return -5;
					break;
				default:
					break;
			}
			return -1;
		}
	}
}