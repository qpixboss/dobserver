﻿using System;
using System.Timers;
using System.Net.Sockets;
using System.Net;
using System.Collections.Generic;
using System.Text;

using dObserver.Receivers;
using dObserver.Utils;
using dObserver.Utils.MKIO;

namespace dObserver.Sources.KPI1 {
	/// <summary>
	/// Класс, описывающий приёмник сигналов от концетратора КПИ-1
	/// </summary>
	class KPI1 : IObservable, IDisposable {
		private uint timeLeft;          // Осталось времени
		private int packageReveived;    // Количество полученных пакетов
		private int port;               // Порт

		private string package;         // Временная переменная для передачи данных между классами

		Timer timer;

		Socket winSocket;
		EndPoint remote;

		List<IObserver> observers;

		InternalDataFormat data;

		FlowAnalysisKPI1 flowAnalysis;

		/// <summary>
		/// Конструктор по умолчанию для 
		/// </summary>
		/// <param name="config"></param>
		/// <param name="sender"></param>
		public KPI1(Config config) {
			observers = new List<IObserver>();
			port = config.Port;

			winSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

			// Повторное использование сокета
			winSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

			winSocket.Bind(new IPEndPoint(IPAddress.Any, port));
			IPEndPoint senderI = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 0);
			remote = senderI;

			if (config.EnTimer) {
				timer = new Timer(1000);        // Секундный интервал
				timer.Elapsed += onTimedEvent;
				timer.AutoReset = true;
				timer.Enabled = false;

				timeLeft = config.Time;
			}

			flowAnalysis = new FlowAnalysisKPI1();

			packageReveived = 0;
		}

		/// <summary>
		/// Оповещение обозревателей.
		/// </summary>
		public void notifyObservers() {
			foreach (IObserver o in observers)
				o.update(data);
		}

		/// <summary>
		/// Добавить обозревателя к системе.
		/// </summary>
		/// <param name="o">Конктерный обозреватель.</param>
		public void registerObserver(IObserver o) {
			observers.Add(o);
		}

		/// <summary>
		/// Удалить обозревателя.
		/// </summary>
		/// <param name="o">Конктерный обозреватель.</param>
		public void removeObserver(IObserver o) {
			observers.Remove(o);
		}

		public void start() {
			if (timeLeft == 0) {
				singlePackage();
			} else {
				ShowMessages.showMessage("Ожидаю начала передачи пакетов по таймеру");
				timer.Enabled = true;
			}
		}

		/// <summary>
		/// Принимаем единичный пакет.
		/// </summary>
		private void singlePackage() {
			ShowMessages.showMessage("Ожидаю начала передачи единичного пакета");

			byte[] dgram = new byte[1024];

			int recv = winSocket.ReceiveFrom(dgram, ref remote);

			ShowMessages.showMessage("Приянт пакет № " + packageReveived.ToString());

			flowAnalysis.inputBytes = dgram;
			flowAnalysis.startAnalysis();
			ShowMessages.showMessage("Обработан пакет № " + packageReveived.ToString());
			package = flowAnalysis.outputString;

			StringBuilder sb = new StringBuilder();
			List<MKIOMessage> list = flowAnalysis.MkioMessage;
			foreach (MKIOMessage m in list)
				sb.Append(m.ToString() + "\n");

			// Складываем все полученные данные в объект класса внутренного формата данных
			// dgram, package, sb, packageReveived
			data = new InternalDataFormat(dgram, package, sb.ToString(), packageReveived);

			// Оповещаем наблюдателей
			notifyObservers();

			packageReveived++;
		}

		private void onTimedEvent(object source, ElapsedEventArgs e) {
			if (timeLeft > 0) {
				byte[] dgram = new byte[1024];

				int recv = winSocket.ReceiveFrom(dgram, ref remote);

				ShowMessages.showMessage("Приянт пакет № " + packageReveived.ToString());

				flowAnalysis.inputBytes = dgram;
				flowAnalysis.startAnalysis();
				ShowMessages.showMessage("Обработан пакет № " + packageReveived.ToString());
				package = flowAnalysis.outputString;

				StringBuilder sb = new StringBuilder();
				List<MKIOMessage> list = flowAnalysis.MkioMessage;
				foreach (MKIOMessage m in list)
					sb.Append(m.ToString() + "\n");

				// Складываем все полученные данные в объект класса внутренного формата данных
				// dgram, package, sb, packageReveived
				data = new InternalDataFormat(dgram, package, sb.ToString(), packageReveived);

				// Оповещаем наблюдателей
				notifyObservers();

				packageReveived++;

				timeLeft -= 1;
			} else {
				timer.Enabled = false;
			}
		}

		public void Dispose() {
			if (winSocket != null) {
				winSocket.Dispose();
			}
		}
	}
}
