﻿using System.Collections.Generic;
using System.Text;
using System.IO;

using dObserver.Receivers;
using dObserver.Utils;
using dObserver.Sources.KPI1;
using dObserver.Utils.MKIO;
using System;

namespace dObserver.Sources {
	/// <summary>
	/// Класс источника, получающего данные от бинарного файла
	/// </summary>
	class BinaryFile : IObservable {
		InternalDataFormat data;
		string path = "binary\\input_binary.bin";
		int packageNumber;

		List<IObserver> observers;

		public BinaryFile(Config conf) {
			observers = new List<IObserver>();
			packageNumber = 0;
		}

		public void notifyObservers() {
			foreach (IObserver o in observers)
				o.update(data);
		}

		public void registerObserver(IObserver o) {
			observers.Add(o);
		}

		public void removeObserver(IObserver o) {
			observers.Remove(o);
		}

		public void start() {
			FlowAnalysisKPI1 flowAnalysis = new FlowAnalysisKPI1();

			using (BinaryReader br = new BinaryReader(File.Open(path, FileMode.Open))) {
				int len = (int)br.BaseStream.Length;

				for (int i = 0; i < len / 1024; i++) {
					byte[] dgram = new byte[1024];
					br.BaseStream.Position = i * 1024;
					br.Read(dgram, 0, 1024);
					flowAnalysis.inputBytes = dgram;
					flowAnalysis.startAnalysis();
					string package = flowAnalysis.outputString;

					StringBuilder sb = new StringBuilder();
					List<MKIOMessage> list = flowAnalysis.MkioMessage;
					foreach (MKIOMessage m in list)
						sb.Append(m.ToString() + "\n");

					// Складываем все полученные данные в объект класса внутренного формата данных
					// dgram, package, sb, packageReveived
					data = new InternalDataFormat(dgram, package, sb.ToString(), packageNumber);

					notifyObservers();

					packageNumber++;

					// Для того, чтобы не сохраняло в один файл потому, что быстро идёт анализ
					System.Threading.Thread.Sleep(1000);

					data = null;
				}
			}
		}

		public void Dispose() {
			
		}
	}
}
