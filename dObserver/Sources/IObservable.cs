﻿using System;
using dObserver.Receivers;

namespace dObserver.Sources {
	/// <summary>
	/// Интерфейс приёмника данных
	/// </summary>
	interface IObservable : IDisposable {
		void registerObserver(IObserver o);
		void removeObserver(IObserver o);
		void notifyObservers();
		void start();
	}
}
